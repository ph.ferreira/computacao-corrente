#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include "timer.h"

float *mat1; //matriz de entrada 1
float *mat2; //matriz de entrada 2
float *saida_seq; //matriz de saida
int nthreads;

typedef struct{
   int id;
   int dim; //dimensao das estruturas de entrada
} tArgs;

void mult_seq(int dim){
  saida_seq = (float *) malloc(sizeof(float) * dim * dim);
  float elemento;

  for(int i=0; i<dim; i++) {
     for(int j=0; j<dim; j++){
        elemento = 0;
        for(int k=0; k<dim; k++){
           elemento += mat1[i*(dim) + k] * mat2[k*(dim) + j];
        }
        saida_seq[i*(dim) + j] = elemento;
     }
   }
}

int main(int argc, char* argv[]) {
   int dim; //dimensao da matriz de entrada
   double inicio, inicio_total, fim, fim_total, delta;

   GET_TIME(inicio_total);
   GET_TIME(inicio);
   //leitura e avaliacao dos parametros de entrada
   if(argc<3) {
      printf("Digite: %s <dimensao da matriz> <numero de threads>\n", argv[0]);
      return 1;
   }
   dim = atoi(argv[1]);
   nthreads = atoi(argv[2]);
   if (nthreads > dim) nthreads=dim;

   //alocacao de memoria para as estruturas de dados
   mat1 = (float *) malloc(sizeof(float) * dim * dim);
   if (mat1 == NULL) {printf("ERRO--malloc\n"); return 2;}
   mat2 = (float *) malloc(sizeof(float) * dim * dim);
   if (mat2 == NULL) {printf("ERRO--malloc\n"); return 2;}
   saida_seq = (float *) malloc(sizeof(float) * dim * dim);
   if (saida_seq == NULL) {printf("ERRO--malloc\n"); return 2;}

   //inicializacao das estruturas de dados de entrada e saida
   for(int i=0; i<dim; i++) {
      for(int j=0; j<dim; j++){
        mat1[i*dim+j] = 1;    //equivalente mat[i][j]
        mat2[i*dim+j] = 1;
        saida_seq[i*dim+j] = 0;
      }
   }
   GET_TIME(fim);
   delta = fim - inicio;
   printf("Tempo inicializacao:%lf\n", delta);

   // multiplicacao sequencial
   GET_TIME(inicio);
   mult_seq(dim);
   GET_TIME(fim);
   delta = fim -inicio;
   printf("Tempo de multiplicacao sequencial: %lf\n", delta);

   //liberacao da memoria
   GET_TIME(inicio);
   free(mat1);
   free(mat2);
   free(saida_seq);
   GET_TIME(fim);
   delta = fim - inicio;
   printf("Tempo finalizacao:%lf\n", delta);

   // Medição to tempo total
   GET_TIME(fim_total);
   delta = fim_total - inicio_total;
   printf("Tempo total:%lf\n", delta);


   return 0;
}
