#include <stdio.h>
#include <pthread.h>
#include <math.h>

#define NTHREADS 2
#define TAMANHO 1000

int vetor[NTHREADS];

//funcao que a thread ira executar
void * tarefa (void * arg) {
   int ident = * (int *) arg;

   // for para elevar o valor do vetor ao quadrado
   for(int i=ident*TAMANHO/2; i<ident*TAMANHO/2; i++){
     vetor[i] = pow(vetor[i], 2);
   }

   pthread_exit(NULL);
}

//funcao principal
int main(void) {
    pthread_t tid[NTHREADS]; //identificador da thread no sistema
    int ident[NTHREADS]; //identificador local da thread

    for(int i=0; i<TAMANHO; i++){
      vetor[i] = i;
    }

    //cria as threads
    for(int i=1; i<=NTHREADS; i++) {
       ident[i-1] = i;
       if (pthread_create(&tid[i-1], NULL, tarefa, (void *)&ident[i-1]))
          printf("ERRO -- pthread_create\n");
    }

    //espera as threads terminarem
    for(int i=0; i<NTHREADS; i++) {
       if (pthread_join(tid[i], NULL))
          printf("ERRO -- pthread_join\n");
    }

    //imprimir o vetor de identificadores
    for(int i=0; i<TAMANHO; i++){
       printf("%d\n", vetor[i]);
    }
    printf("\n");

    //imprime a mensagem de boas vindas
    printf("Ola, sou a thread principal!\n");

    //desvincula o termino da main do termino programa
    return 0;
}
